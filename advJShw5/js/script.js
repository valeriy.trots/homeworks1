const btn = document.querySelector('.btn')
const  rec = async () => {
    let response = await fetch('https://api.ipify.org/?format=json')
    let data = await response.json()
    const secondResponse = await fetch('http://ip-api.com/json/78.159.44.124')
    let dataIP = await secondResponse.json()
    deleteElement()
    showElement(dataIP)
}
btn.addEventListener('click', () => {
rec()
})
const showElement = (dataShow) => {
    const {timezone, country ,regionName, city, zip} = dataShow
    document.body.insertAdjacentHTML('beforeend', `
    <div>
    <p>Timezone: ${timezone}</p>
    <p>Country: ${country}</p>
    <p>RegionName: ${regionName}</p>
    <p>City: ${city}</p>
    <p>Zip: ${zip}</p>
    </div>
`)
}
const deleteElement = () => {
    const div = document.querySelector('div')
    if (div !== null) {
        div.remove()
    }
}
