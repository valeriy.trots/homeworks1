const body = document.querySelector('.body');
const posts = document.querySelector('.most-popular-posts');
const btn = document.querySelector('.btn');

const colorBack = 'yellow';
function background() {
    body.classList.toggle('body-color');
    posts.classList.toggle('most-popular-posts-color');

    if (body.classList.contains('body-color')) {
        localStorage.setItem('color', 'color');
    } else {
        btn.style.color = colorBack;
        localStorage.removeItem('color');
    }
}
btn.addEventListener('click', background);
if (localStorage.getItem('color')) {
    body.classList.add('body-color');
    posts.classList.add('most-popular-posts-color');
} else {
    body.classList.remove('body-color');
    posts.classList.remove('most-popular-posts-color');
    btn.style.color = colorBack;
}