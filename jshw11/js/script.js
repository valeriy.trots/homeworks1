function tabsBtn() {
    const tabNav = document.querySelectorAll('.btn');
    document.onkeydown = function (event) {
        tabNav.forEach(item => {
            // if (event.code === item.id) {
            if (event.key.toLowerCase() === item.id) {
                return item.classList.add('active');
            }
            item.classList.remove('active');
        });
    };
}
tabsBtn();