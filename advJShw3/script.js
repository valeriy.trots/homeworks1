class Employee {
    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
    }
   get getName(){
        return this.name
    }
    set setName(firstName){
        return this.name = firstName
    }
    get getAge(){
        return this.age
    }
    set setAge(newAge){
        return this.age = newAge
    }
    get getSalary(){
        return this.salary
    }
    set setSalary(newSalary){
        return this.salary = newSalary
    }
}
const employee = new Employee('valeriy',65, 1980 )
// console.log(employee)
// console.log(employee.getName)
// console.log(employee.setName = 'Alexey')
// console.log(employee)

const lang = ['franche','english', 'russian']

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang
    }
    get getSalary() {
        return this.salary * 3
    }
}
const firstProgrammer = new Programmer('Valeriy',65,1980, lang)
const secondProgrammer = new Programmer('Alexey', 22, 10000, ['sweden','portugal','ukranian'])
console.log(firstProgrammer,secondProgrammer)
console.log(firstProgrammer.getSalary,secondProgrammer.getSalary)