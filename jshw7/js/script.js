
function watchArray(array, parent = document.body) {
    const list = document.createElement('ul');
    document.body.prepend(list);
    array.map((item) => {list.insertAdjacentHTML( 'beforeend',`<li>${item}</li>`);
    })
}
watchArray(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);



const cityList = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
function cityArray(arr, parent = document.body) {
const cityListElement = document.createElement('ul');
    parent.prepend(cityListElement)
    arr.map((item) => {const li = document.createElement('li');
        cityListElement.append(li);
        li.innerHTML = item
    })
}
cityArray(cityList);

