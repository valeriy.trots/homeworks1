const inputElement = document.querySelector('#input');
const formElement = document.querySelector('#form');

inputElement.addEventListener('focus', function () {
    inputElement.classList.add('focus');
});
inputElement.addEventListener('blur', function () {
    inputElement.classList.remove('focus');
});
const span = document.createElement('span');
const btn = document.createElement('button');

function PriceOn() {
    if (inputElement.value >= 0) {
        inputElement.classList.remove('focus-red');
        inputElement.classList.add('focus');
        inputElement.style.color = 'green';
        inputElement.style.fontWeight = 'bold';
        formElement.before(span);
        span.innerHTML = `Текущая цена: ${inputElement.value}`;

        span.after(btn);
        btn.innerHTML = 'x';
        btn.style.display = 'inline-block';

    } else {
        inputElement.classList.remove('focus');
        inputElement.classList.add('focus-red');
        formElement.after(span);
        span.innerHTML = 'Please enter correct price';
        btn.style.display = 'none';
    }  
}

function delet () {
    span.remove();
    btn.remove();
    inputElement.value = '';
}
btn.addEventListener('click', delet);
inputElement.addEventListener('blur', PriceOn);
