import React from 'react';
import PropTypes from 'prop-types';
import {Cart} from "./Cart";
import './products.scss';


export const CartList = ({
                             products,
                             handleModalOpen,
                             handleAddToFavorites,
                             handleRemoveFromFavorites,
                         }) => {

    return (
        <div className="container">
            {products.map((product) => (
                <div key={product.id} className="productItem">
                    <Cart {...product} handleModalOpen={handleModalOpen} handleAdd={handleAddToFavorites}
                          handleRemove={handleRemoveFromFavorites}/>
                </div>
            ))}
        </div>
    );
}

CartList.propTypes = {
    products: PropTypes.array,
    onClick: PropTypes.func,
    handleModalOpen: PropTypes.func,
}
