(function () {
    const galleryBtnDropMenu = document.querySelector('.gallery-btn');
    const loadingGallery = document.querySelector('.gallery-cssload-loader');
    const galleryItemsDrop = document.querySelector('.gallery-two');
    const showDropGallery  = () => {
        galleryBtnDropMenu.style.display = 'none';
        loadingGallery.style.display = 'block';
        setTimeout(() => {
            loadingGallery.style.display = 'none';
            galleryItemsDrop.style.display = 'grid'
        }, 1000)
    };
    galleryBtnDropMenu.addEventListener('click', showDropGallery);
}());