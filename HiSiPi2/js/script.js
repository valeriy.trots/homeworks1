//секция два табс
let buttons = document.querySelector('.services-menu');
const color1= '#18CFAB';
const color2 = '#F8FCFE';

function changeClick(event) {
    if (event.target.className == 'services-menu-item') {
        let dataTab = event.target.getAttribute('data-tab');
        let content = document.querySelectorAll('.services-menu-content-item');
        let tabsTitle = document.querySelectorAll('.services-menu-item');
        for (let i = 0; i < tabsTitle.length; i++ ) {
            tabsTitle[i].classList.remove('services-menu-item-active');
        }
        for (let i = 0; i < content.length; i++) {
            if (dataTab == i) {
                content[i].style.display = 'flex';
                tabsTitle[i].classList.add('services-menu-item-active');
                for (let i = 0; i < tabsTitle.length; i++) {
                    if (dataTab == i) {
                        tabsTitle[i].style.backgroundColor = color1;
                    } else {
                        tabsTitle[i].style.backgroundColor = color2;
                    }
                }
            } else {
                content[i].style.display = 'none';
            }
        }
    }
};
buttons.addEventListener('click', changeClick);
//секция три фильтр
(function() {
    const worksItems = document.querySelectorAll('.works-item');
    const menu = document.querySelector('.works-menu');
    const works = document.querySelectorAll('.works-menu-item');

    const arr = document.querySelector('.works-items');
    const arr1 = document.querySelector('#works-add1');
    const arr2 = document.querySelector('#works-add2');

    const filter = (event) => {
        if (event.target.tagName !== 'LI') return false;
        const filter = event.target.dataset['filter'];
        worksItems.forEach(item => {
            item.classList.remove('works-hide');
            if (!item.classList.contains(filter) && filter !== 'all-items') {
                item.classList.add('works-hide');
            }
            arr.style.marginLeft = '100px';
            arr1.style.marginLeft = '100px';
            arr2.style.marginLeft = '100px'
        })
    };
    const changeColor= (event) => {
        if (event.target.tagName !== 'LI') return false;
        for (let menuItem of works) {
            menuItem.classList.remove('works-active')
        }
        event.target.classList.add('works-active');
    };
    menu.addEventListener('click', filter);
    menu.addEventListener('click', changeColor);
}());
//секция три плюс-кнопка
const add1 = document.querySelector('#btn-add1');
const add2 = document.querySelector('#btn-add2');
const worksAdd1 = document.querySelector('#works-add1');
const worksAdd2 = document.querySelector('#works-add2');

const load = document.querySelector('.works-cssload-loader');

const show1  = () => { add1.style.display = 'none';
    load.style.display = 'block';
    setTimeout(() => { load.style.display = 'none';
        worksAdd1.style.display = 'grid';
        add2.style.display = 'block';
    }, 1000)};
const show2  = () => { add2.style.display = 'none';
    load.style.display = 'block';
    setTimeout(() => { load.style.display = 'none';
        worksAdd2.style.display = 'grid';
    }, 1000)};

add1.addEventListener('click', show1);
add2.addEventListener('click', show2);
//секция пять слайдер
let galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 1,
    slidesPerView: 4,
    loop: true,
    centeredSlides: true,
    freeMode: true,
    loopedSlides: 4, //looped slides should be the same
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    navigation: {
        nextEl: '.button-next',
        prevEl: '.button-prev'
    }
});
let galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 1,
    loop: true,
    freeMode: true,
    loopedSlides: 1, //looped slides should be the same
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    navigation: {
        nextEl: '.button-next',
        prevEl: '.button-prev'
    },
    thumbs: {
        swiper: galleryThumbs
    }
});
