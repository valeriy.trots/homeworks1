// import React,{Component} from "react"
// import Button from "./Button";
// import "./style.scss"
//
// class Modal extends Component {
//     constructor(props) {
//         super(props);
//     }
//
//     // renderCloseButton() {
//     //     if (this.props.closeButton === true) {
//     //         return <Button text="x" className="close" onClick={this.props.onCancel}/>
//     //     }
//     // }
//
//     render() {
//         // console.log(this.props)
//         return (
//             <>
//                 {this.props.isOpen &&
//                 <div className="modal">
//                     <div className="modalBackDrop" onClick={this.props.onCancel}/>
//                     <div className="modalDialog">
//                         <div className="modalContent">
//                             <div className="modalHeader">
//                                 <h3 className="modalTitle">{this.props.title}</h3>
//                                 <Button text='X' onClick={this.props.onCancel}/>
//                                 {/*{this.renderCloseButton()}*/}
//                             </div>
//                             <div className="modal-body">
//                                 <div className="ModalBody">
//                                     {this.props.title}
//                                 </div>
//                             </div>
//                             <div className="ModalFooter">
//                                 <Button onClick={this.props.onCancel} text='Cancel' color='red'/>
//                                 {/*<Button onClick={this.secondModalOpen} text = 'cancel' color = 'red'/>*/}
//                                 <Button className='btn primaryButton' text='Submit'
//                                         onClick={this.props.onSubmit(this.props.id)}/>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//                 }
//             </>
//         )
//     }
// }
// export default Modal


import React from 'react';
import Button from "./Button";
import './style.scss'
import PropTypes from "prop-types";

class Modal extends React.Component {

    render() {
        return (
            <>
                {this.props.isOpen &&
                <div className="modal">
                    <div className="modalBackDrop" onClick={this.props.onCancel}/>
                    <div className="modalDialog">
                        <div className="modalContent">
                            <div className="modalHeader">
                                <h3 className="modalTitle">{this.props.title}</h3>
                                <Button text="x" className="close" onClick={this.props.onCancel}/>
                            </div>
                            <div className="modal-body">
                                <div className="ModalBody">
                                    {this.props.children}
                                    {/*{this.props.id}*/}
                                </div>
                            </div>
                            <div className="ModalFooter">
                                <Button onClick={this.props.onCancel} className="btn secondaryButton" text='Cancel'/>
                                <Button className='btn primaryButton' text='Submit' onClick={() => {this.props.onSubmit(this.props.id)}}/>
                            </div>
                        </div>
                    </div>
                </div>
                }
            </>
        )
    }
}

Modal.propTypes = {
    title: PropTypes.string,
    // children: PropTypes.string,
    id: PropTypes.string,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    isOpen: PropTypes.bool,
}

export default Modal