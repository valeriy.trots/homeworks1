import React,{Component} from "react"

class Button extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <button onClick={this.props.onClick} style={{backgroundColor:this.props.color}}>{this.props.text}</button>

        )
    }
}
export default Button
