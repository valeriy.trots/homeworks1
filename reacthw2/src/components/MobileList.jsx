import React,{Component} from "react";
import Mobile from "./Mobile";
import PropTypes from "prop-types";
import Modal from "./Modal";

class MobileList extends Component {
    constructor(props) {
        super(props);
        console.log(props)
    }
    render() {
        const {products, modalOpen, onClick} = this.props
        // console.log(this.props)
        return (
<div className={'blockCards'}>
    {
        products.map((product) => (
            <div key={product.id} className={'cards'}>
                <Mobile {...product} modalOpen = {modalOpen} onClick ={onClick}/>
            </div>
        ))
    }

</div>
        )
    }
}

MobileList.propTypes = {
    title: PropTypes.string,
    // children: PropTypes.string,
    id: PropTypes.string,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    isOpen: PropTypes.bool,
}

export default MobileList