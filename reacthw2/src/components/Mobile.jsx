import React,{Component} from "react";
import Button from "./Button";
import StarBorderIcon from '@material-ui/icons/StarBorder';
import {withMobileDialog} from "@material-ui/core";
import PropTypes from "prop-types";
import "./style.scss"
// import Modal from "./Modal";

class Mobile extends Component {
    constructor(props) {
        super(props);
        console.log(props);
    }
    render() {
        return (
            <div className={'mobilStyle'}>
                <div className={'imgStar'}>
                <img src={this.props.img}  className={'imageScss'} alt=""/>
                <StarBorderIcon onClick={() => this.props.onClick(this.props.id)} className={this.props.isInFavorites?'starActive':''}/>
                </div>
                {/*<img src={this.props.img}  className={'imageScss'} alt=""/>*/}
                <p>Name:{this.props.name}</p>
                <p>Price:{this.props.price}</p>
                <p>Articul:{this.props.articul}</p>
                <p>Color:{this.props.color}</p>
                <Button onClick={() => this.props.modalOpen(this.props.id)} text={' Add to cart'}/>
            </div>

        )
    }
}

Mobile.propTypes = {
    title: PropTypes.string,
    // children: PropTypes.string,
    id: PropTypes.string,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    isOpen: PropTypes.bool,
}

export default Mobile
