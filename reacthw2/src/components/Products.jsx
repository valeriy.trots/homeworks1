import React,{Component} from "react";
import MobileList from "./MobileList";
import Modal from "./Modal";
import Button from "./Button";

class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            listProducts: [],
            currentId: null,
            currentProduct:false,
            isLoading: false,
            error: false
        }
    }
async getProducts () {
        try {
            const response = await fetch(`./database.json`)
            const data = await response.json();
            // console.log(data)
            const productsInCart = JSON.parse(localStorage.getItem('cart'));
            const favorites = JSON.parse(localStorage.getItem('favorites'));
            const newProducts = this.checkProducts(data.list,productsInCart,favorites);
            console.log(newProducts)
            this.setState({...this.state,listProducts:newProducts})
        }
        catch (e){
            console.log('error', e)
            this.setState({...this.state,error:true});
        }
}
checkProducts(products,productsInCart,favorites) {
    console.log(products)
        return   products.map((product) => {
            const cart = productsInCart.some(({id}) => (id === product.id))
            const productsInFavorites = favorites.some(({id}) => (id === product.id))
            return {
                ...product,
                isInCart:cart,
                isInFavorites:productsInFavorites
            }

        })
    // return newProducts
}
    async componentDidMount() {
        if (!localStorage.getItem('cart') || !localStorage.getItem('favorites')) {
            localStorage.setItem('cart', JSON.stringify([]))
            localStorage.setItem('favorites', JSON.stringify([]))
        }
        const data = await this.getProducts()
    }
// async componentDidMount() {
//         if (!localStorage.getItem('cart') || !localStorage.getItem('favorites')) {
//             localStorage.setItem('cart', JSON.stringify([]))
//             localStorage.setItem('favorites', JSON.stringify([]))
//         }
//         await this.getProducts()
// }

    modalOpen(id) {
        if(id){
            this.setState((state) => ({
                ...state,
                modal: !state.modal,
                currentId: id
            }))
        } else {
            this.setState((state) => ({
                ...state,
                modal: !state.modal,
                currentId: null
            }))
        }
    }
    addToCart(id) {
        const cart = JSON.parse(localStorage.getItem('cart'));
        const product = this.state.listProducts.find((product) => product.id === id);
        cart.push(product);
        // console.log(cart,product)
        localStorage.setItem('cart', JSON.stringify(cart));
        this.setState((state) => ({
            ...state,
            modal: !state.modal,
            currentId: null
        }))
    }
    addToFavorites(id) {
        if(id){
            const favorites = JSON.parse(localStorage.getItem('favorites'))
            const product = this.state.listProducts.find((product) => product.id === id);
            favorites.push(product);
            // console.log(cart,product)
            localStorage.setItem('favorites', JSON.stringify(favorites));
            this.setState((state) => ({
                ...state,
                currentProduct: !state.currentProduct,
                // modal: !state.modal,
                currentId: id
            }))
        }
        //     const favorites = JSON.parse(localStorage.getItem('favorites'))
        // const product = this.state.listProducts.find((product) => product.id === id);
        // favorites.push(product);
        // // console.log(cart,product)
        // localStorage.setItem('favorites', JSON.stringify(favorites));
        // this.setState((state) => ({
        //     ...state,
        //     modal: !state.modal,
        //     currentId: id
        // }))
        }

    render() {
        return (
             <div>
                 <Modal isOpen = {this.state.modal} onSubmit = {(id) => this.addToCart(id)} onCancel = {() => this.modalOpen()} title = {' Add to cart'} id = {this.state.currentId}/>
                 <MobileList products = {this.state.listProducts} modalOpen = {this.modalOpen.bind(this)} onClick = {(id) => this.addToFavorites(id)}/>
             </div>
        )
    }
}
export default Products