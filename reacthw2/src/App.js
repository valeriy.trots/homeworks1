import React,{Component} from "react"

import './App.css';
import Products from './components/Products';

class App extends Component{
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div>
                <Products/>
            </div>
        );
    }
}

export default App;
