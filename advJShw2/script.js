const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
const list = document.querySelector('#root');
const ul = document.createElement("ul");

const  newBooks = books.map(function (item) {
    const {author, name, price} = item;

    try {
        if (author && name && price) {
            return `<li>${author}</li>
            <li>${name}</li>
            <li>${price}</li>`
        } else {
            if(!author) {
                throw new Error("Author property does not exist")
            } else if (!name){
                throw new Error("Name property does not exist")
            } else if(!price){
                throw new Error("Price property does not exist")
            }
        }
    } catch (e) {
        console.error(e)
    }


})
const booksString = newBooks.join(" ");
ul.insertAdjacentHTML("afterbegin",booksString)
list.append(ul);
