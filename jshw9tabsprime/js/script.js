let btn = document.querySelector('.tabs');
const tabContentItem = document.querySelectorAll('.tab-content-item');
const tabsTitle = document.querySelectorAll('.tabs-title');
function tabs (event) {
    if (event.target.className == 'tabs-title') {
        const dataTab = event.target.getAttribute('data-tab');
        for (let i = 0; i < tabsTitle.length; i++ ) {
            tabsTitle[i].classList.remove('tabs-title-active');
        }
        for (let i = 0; i < tabContentItem.length; i++) {
            if (dataTab == i) {
                tabContentItem[i].style.display = 'block';
                for (let i = 0; i < tabsTitle.length; i++) {
                    if (dataTab == i) {
                        tabsTitle[i].style.backgroundColor = '#937341';
                        tabsTitle[i].style.color ='#23201D';
                    } else {
                        tabsTitle[i].style.backgroundColor = '#23201D';
                        tabsTitle[i].style.color = '#937341';
                    }
                }
            } else {
                tabContentItem[i].style.display = 'none';
            }
        }
    }
}
btn.addEventListener('click', tabs);