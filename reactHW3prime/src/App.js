import React,{Component} from "react"
import './App.css';
import AppRoutes from './components/AppRoutes'

const App = () => {
        return (
            <div>
                <AppRoutes/>
            </div>
        );
    }
export default App;
