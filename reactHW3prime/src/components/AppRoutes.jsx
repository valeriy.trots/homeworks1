import React from 'react';
import {BrowserRouter as Router, Link} from 'react-router-dom'
import Products from "./Products";

const AppRoutes = () => {
        return (
            <div>
                <Link to="/">MobilTelephon</Link>
                <Link to="/Cart">Cart</Link>
                <Link to="/Favorites">Favorites</Link>
                <Products/>
            </div>
        )
}
export default AppRoutes;
