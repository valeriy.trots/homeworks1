import React from "react";
import Button from "./Button";
import StarBorderIcon from '@material-ui/icons/StarBorder';
import {withMobileDialog} from "@material-ui/core";
import PropTypes from "prop-types";
import "./style.scss"

const Cart = (props) => {
    console.log(props);
    return (
        <div className={'mobilStyle'}>
            <Button onClick={() => props.modalOpen(props.id)} text={'X'}/>
            <div className={'imgStar'}>
                <img src={props.img}  className={'imageScss'} alt=""/>
                <StarBorderIcon onClick={() => props.onClick(props.id)} className={props.isInFavorites?'starActive':''}/>
            </div>
            <p>Name:{props.name}</p>
            <p>Price:{props.price}</p>
            <p>Articul:{props.articul}</p>
            <p>Color:{props.color}</p>
            <Button  text={'Buy'}/>
        </div>

    )
}

Cart.propTypes = {
    title: PropTypes.string,
    // children: PropTypes.string,
    id: PropTypes.string,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    isOpen: PropTypes.bool,
}

export default Cart;
