import React from "react";
import Mobile from "./Mobile";
import PropTypes from "prop-types";
import "./style.scss";

const MobileList = (props) => {

        const {products, modalOpen, onClick,removeFromFavorites} = props
        return (
          <div className={'blockCards'}>{
                products.map((product) => (
                    <div key={product.id} className={'cards'}>
                        <Mobile {...product} modalOpen = {modalOpen} onClick ={onClick} handleRemove = {removeFromFavorites}/>
                    </div>
                ))
            }

          </div>
        )
}

MobileList.propTypes = {
    title: PropTypes.string,
    // children: PropTypes.string,
    id: PropTypes.string,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    isOpen: PropTypes.bool,
}

export default MobileList