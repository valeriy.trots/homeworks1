import React from 'react';
import Button from "./Button";
import './style.scss'
import PropTypes from "prop-types";

const Modal = (props) => {

        return (
            <>
                {props.isOpen &&
                <div className="modal">
                    <div className="modalBackDrop" onClick={props.onCancel}/>
                    <div className="modalDialog">
                        <div className="modalContent">
                            <div className="modalHeader">
                                <h3 className="modalTitle">{props.title}</h3>
                                <Button text="x" className="close" onClick={props.onCancel}/>
                            </div>
                            <div className="modal-body">
                                <div className="ModalBody">
                                    {props.children}
                                    {/*{this.props.id}*/}
                                </div>
                            </div>
                            <div className="ModalFooter">
                                <Button onClick={props.onCancel} className="btn secondaryButton" text='Cancel'/>
                                <Button className='btn primaryButton' text='Submit' onClick={() => {props.onSubmit(props.id)}}/>
                            </div>
                        </div>
                    </div>
                </div>
                }
            </>
        )
}

Modal.propTypes = {
    title: PropTypes.string,
    // children: PropTypes.string,
    id: PropTypes.string,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    isOpen: PropTypes.bool,
}

export default Modal