import React,{useState,useEffect} from "react";
import {Route,Switch,BrowserRouter as Router} from "react-router-dom"
import MobileList from "./MobileList";
import Modal from "./Modal";
import Button from "./Button";
import CartList from "./CartList";


const Products = (props) => {
        const [modal,setModal] = useState(false);
        const [listProducts,setListProducts] = useState([]);
        const [currentId,setCurrentId] = useState(null);
        const [currentProducts,setCurrentProducts] = useState(false);
        const [isLoading,setIsLoading] = useState(false);
        const [error,setError] = useState(false);

async function getProducts() {
    try {
        const response = await fetch(`./database.json`)
        const data = await response.json();
        const productsInCart = JSON.parse(localStorage.getItem('cart'));
        const favorites = JSON.parse(localStorage.getItem('favorites'));
        const newProducts = checkProducts(data.list, productsInCart, favorites);
        console.log(newProducts)
        setListProducts(newProducts);
    } catch (e) {
        console.log('error', e)
        setError(true);
    }
}

 function checkProducts(products,productsInCart,favorites) {
        return   products.map((product) => {
            const cart = productsInCart.some(({id}) => (id === product.id))
            const productsInFavorites = favorites.some(({id}) => (id === product.id))
            return {
                ...product,
                isInCart:cart,
                isInFavorites:productsInFavorites
            }

        })
}

    useEffect(() => {
        if (!localStorage.getItem('cart') || !localStorage.getItem('favorites')) {
            localStorage.setItem('cart', JSON.stringify([]))
            localStorage.setItem('favorites', JSON.stringify([]))
        }
        const data = getProducts()
    }, []);

   function modalOpen(id) {
        if(id){
            setModal(!modal)
            setCurrentId(id)
            console.log(listProducts)
        } else {
            setModal(!modal)
            setCurrentId(null)
        }
    }

   function addToCart(id) {
        const cart = JSON.parse(localStorage.getItem('cart'));
        const product = listProducts.find((product) => product.id === id);
        cart.push(product);
        localStorage.setItem('cart', JSON.stringify(cart));
       setModal(!modal)
       setCurrentId(null)
       setCurrentProducts(id)
       const cartList = listProducts.map(product => {
           if(id===product.id) {
               product.isInCart=true
           }
           return product
       })
       setListProducts(cartList)
   }

   function addToFavorites(id) {
            const favorites = JSON.parse(localStorage.getItem('favorites'))
            const product = listProducts.find((product) => product.id === id);
            favorites.push(product);
            localStorage.setItem('favorites', JSON.stringify(favorites));
            setCurrentId(id)
            setCurrentProducts(!currentProducts)
       const favoritesList = listProducts.map(product => {
           if(id===product.id) {
               product.isInFavorites=true
           }
           return product
       })
       setListProducts(favoritesList)
   }

function removeFromCart(id) {
    const cart = JSON.parse(localStorage.getItem('cart'));
    cart.map((item,index) => {
        if(item.id===id) {
            cart.splice(index,1)
        }
        return cart

    })
    localStorage.setItem('cart', JSON.stringify(cart));
    setModal (!modal)
   const cartList = listProducts.map(product => {
        if(product.id===id) {
            product.isInCart =false
        }
        return product
    })
    setListProducts(cartList)
}

function removeFromFavorites(id) {
       setCurrentProducts(!currentProducts)
    const favorites = JSON.parse(localStorage.getItem('favorites'))
    favorites.map((item,index) => {
        if (id===item.id) {
             favorites.splice(index,1)
        }
        return favorites
    })
    localStorage.setItem('favorites', JSON.stringify(favorites));
    const favoritesList = listProducts.map(product => {
        if(product.id===id) {
            product.isInFavorites =false
        }
        return product
    })
    setListProducts(favoritesList)
}

        return (
            <Switch>
                <Route exact path = '/'>
                    <Modal isOpen = {modal} onSubmit = {(id) => addToCart(id)} onCancel = {() => modalOpen()} title = {' Add to cart'} id = {currentId}/>
                    <MobileList products = {listProducts} modalOpen = { (id) => modalOpen(id)} onClick = {(id) => addToFavorites(id)} removeFromFavorites={(id) => removeFromFavorites(id)}/>
                </Route>
                <Route path = "/Cart">
                    <Modal isOpen = {modal} onSubmit = {(id) => removeFromCart(id)} onCancel = {() => modalOpen()} title = {' Remove to cart'} id = {currentId}/>
                    <CartList products = {listProducts.filter(product => {return product.isInCart})}  modalOpen = {(id) => modalOpen(id)} onClick = {(id) => addToFavorites(id)}
                              removeFromFavorites={(id) => removeFromFavorites(id)} />
                </Route>
                <Route path = "/Favorites">
                    <MobileList products = {listProducts.filter(product => {
                        if(product.isInFavorites){
                            console.log(product)
                        }
                        return product.isInFavorites})} modalOpen = { (id) => modalOpen(id)} onClick = {(id) => addToFavorites(id)}
                                removeFromFavorites={(id) => removeFromFavorites(id)}/>
                </Route>
            </Switch>
        )
}
export default Products