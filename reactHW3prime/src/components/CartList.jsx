import React from "react";
import Cart from "./Cart";
import PropTypes from "prop-types";

const CartList = (props) => {
    console.log(props)

    const {products, modalOpen, onClick} = props
    return (
        <div className={'blockCards'}>{
            products.map((product) => (
                <div key={product.id} className={'cards'}>
                    <Cart {...product} modalOpen = {modalOpen} onClick ={onClick}/>
                </div>
            ))
        }

        </div>
    )
}

CartList.propTypes = {
    title: PropTypes.string,
    // children: PropTypes.string,
    id: PropTypes.string,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    isOpen: PropTypes.bool,
}

export default CartList