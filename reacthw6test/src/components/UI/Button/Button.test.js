import Button from "./Button";
import {render} from "@testing-library/react";
import userEvent from "@testing-library/user-event";

describe('testing modal.js', () =>{

    test('render check', () =>{
        render(<Button/>)
    })
    test('props checking', () => {
        const controlText = 'Add to cart'
        const {getByText} = render(<Button text={controlText}/>)
        getByText(controlText)
        expect(getByText(controlText)).toBeDefined()
    })
    test('clicking button', () => {
        const mock = jest.fn()
        const {getByTestId} = render(<Button products={{id: 1}} onClick= {mock}/>)
        const addBtn = getByTestId('btn')
        userEvent.click(addBtn)
        expect(mock).toHaveBeenCalledTimes(1)
    })

})