import Modal from './Modal'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { render } from '@testing-library/react'
import { rootReducer } from '../../../store/rootReducer'
import configureStore from "redux-mock-store";

const middlewares = [];
const mockStore = configureStore(middlewares);
const openBtn = () => ({ type: "OPEN_MODAL_FOR_ADD_PRODUCT_TO_CART" });
const closeBtn = () => ({ type: "CLOSE_MODAL_FOR_ADD_TO_CART" });

describe('Modal component', () => {
    it('should render correctly', () => {
        let store = createStore(rootReducer, {
            products: [],
            isOpenModalForAddToCart: false,
            dataForModalAddProductToCart: [],
            cartProducts: [],
            isOpenModalForDeleteProductWithCart: false,
            productForModalDeleteWithCart: [],
            favoriteProducts: [],
        })
        render(<Provider store={store}><Modal dataForModalCard={{color:'red'}}/></Provider>)
    })
    it("should render correctly with header", () => {
        let store = createStore(rootReducer, {
            products: [],
            isOpenModalForAddToCart: false,
            dataForModalAddProductToCart: [],
            cartProducts: [],
            isOpenModalForDeleteProductWithCart: false,
            productForModalDeleteWithCart: [],
            favoriteProducts: [],
        });
        const header = "test";
        render(
            <Provider store={store}>
                <Modal header={header} dataForModalCard={{color:'red'}}/>
            </Provider>
        );
    });

    it('should render correctly with dataForModalCard', () => {
        let store = createStore(rootReducer, {
            products: [],
            isOpenModalForAddToCart: false,
            dataForModalAddProductToCart: [],
            cartProducts: [],
            isOpenModalForDeleteProductWithCart: false,
            productForModalDeleteWithCart: [],
            favoriteProducts: [],
        })
        const { getByText } = render(<Provider store={store}><Modal dataForModalCard={{color: 'Test'}}/></Provider>)
        getByText('Test')
    })
    it("should pass open action", () => {
        const initialState = {};
        const store = mockStore(initialState);
        store.dispatch(openBtn());
        const actions = store.getActions();
        const expectedPayload = { type: "OPEN_MODAL_FOR_ADD_PRODUCT_TO_CART" };
        expect(actions).toEqual([expectedPayload]);
    });
    it("should pass close action", () => {
        const initialState = {};
        const store = mockStore(initialState);
        store.dispatch(closeBtn());
        const actions = store.getActions();
        const expectedPayload = { type: "CLOSE_MODAL_FOR_ADD_TO_CART" };
        expect(actions).toEqual([expectedPayload]);
    });
})
