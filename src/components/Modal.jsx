import React,{PureComponent} from "react"
import Button from "./Button";
import "./style.scss"
class Modal extends PureComponent {
    constructor(props) {
        super(props);
    }
    renderCloseButton() {
        if(this.props.closeButton === true) {
            return <Button text="x" className="close" onClick={this.props.onCancel}/>
        }
    }
    render() {
        return (
            <>
            {this.props.isOpen &&
                    <div className="modal">
                        <div className="modalBackDrop" onClick={this.props.onCancel}/>
                        <div className="modalDialog">
                            <div className="modalContent">
                                <div className="modalHeader">
                                    <h3 className="modalTitle">{this.props.header}</h3>
                                    { this.renderCloseButton()}
                                </div>
                                <div className="modal-body">
                                    <div className="ModalBody">
                                        {this.props.text}
                                    </div>
                                </div>
                                <div className="ModalFooter">
                                    <Button onClick={this.props.onCancel} text = 'Cancel' color = 'red'/>
                                    {/*<Button onClick={this.secondModalOpen} text = 'cancel' color = 'red'/>*/}
                                    <Button className='btn primaryButton' text='Submit'/>
                                </div>
                            </div>
                        </div>
                    </div>
            }
    </>
        )
    }
}
export default Modal