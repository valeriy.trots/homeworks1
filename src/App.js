import React,{PureComponent} from "react"

import './App.css';
import Button from "./components/Button";
import Modal from  "./components/Modal";

const firstText = "Open first modal";
const secondText = "Open second modal";
const bgColorFirst = 'blue';
const bgColorSecond = 'green';
class App extends PureComponent{
    constructor(props) {
        super(props);
        this.state = {
            firstModal: false,
            secondModal: false
        }
    }
    firstModalOpen() {
        this.setState((state) => ({
            ...state,
            firstModal: !state.firstModal
        }))
    }
    secondModalOpen() {
        this.setState((state) => ({
            ...state,
            secondModal: !state.secondModal
        }))
    }
    render() {
        return (
            <div className="App">
                {
                    this.state.firstModal &&
                    <Modal
                        isOpen={this.state.firstModal}
                        header='first Modal'
                        text='Do you want to delete this file?'
                        closeButton={false}
                        onCancel={() => this.firstModalOpen()}/>
                }
                {
                    this.state.secondModal && <Modal
                        isOpen = {this.state.secondModal}
                        header = 'second Modal'
                        text='Do you want to create this file?'
                        closeButton = {true}
                        onCancel = {() => this.secondModalOpen()}/>
                }
                <Button onClick={this.firstModalOpen.bind(this)} text = {firstText} color = {bgColorFirst}/>
                <Button text = {secondText} color = {bgColorSecond} onClick={this.secondModalOpen.bind(this)}/>
            </div>
        );
    }
}

export default App;
