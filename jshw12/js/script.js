
let textTime = document.querySelector('.text-time');
let imagesShow = document.querySelectorAll('.image-to-show');
let buttons= document.querySelector('.buttons')

let current = 1;
let intervalFirst;
let intervalSecond;
function showingImages() {
    for (let i = 0; i < imagesShow.length; i++) {
        imagesShow[i].classList.add('image-hide');
    }
    imagesShow[current].classList.remove('image-hide');
    if (current === imagesShow.length - 1) {
        current = 0;
    } else { current ++}
}
let stopSlider = document.querySelector('.stop');
let startSlider = document.querySelector('.start');
function slider() {
    intervalFirst = setInterval(showingImages, 3000);
    startSlider.disabled = true;
}
slider();
let counter = 3;
function counterSlide () {
    counter--;
    textTime.innerHTML = `Время до следующего слайда<span>${counter}<span>`
    if (counter === 1) { counter = 4 }
}
function newCounter() {
    intervalSecond = setInterval(counterSlide, 1000);
}
newCounter()
setTimeout(() => {
    buttons.classList.add('show-btn');
}, 3000);

stopSlider.addEventListener('click', () => {
    startSlider.disabled = false;
    clearInterval(intervalFirst);
    clearInterval(intervalSecond);
});
startSlider.addEventListener('click', () => {
    slider();
    newCounter();
});

