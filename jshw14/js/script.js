$(function () {
    $(('a[href=""]')).on("click", function(event){
        const anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 1000);
        event.preventDefault();
        return false;
    });
});
$(function() {
    const btnToggle = $('.btn-toggle');
    btnToggle.on('click', function() {
        $('.posts').toggle('.posts-hide')
    })
    btnToggle.click(function(){
        if (!$(this).data('status')) {
            $(this).html('SHOW POSTS');
        }
        else {
            $(this).html('HIDE POSTS');
        }
    });
});
$(document).ready(function() {
    const buttonTop = $('.btn-top');
    $(window).scroll (function () {
        if ($(this).scrollTop () > 300) {
            buttonTop.fadeIn();
        } else {
            buttonTop.fadeOut();
        }
    });
    buttonTop.on('click', function(){
        $('body, html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});
