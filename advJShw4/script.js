const divCont = document.createElement('div')
const ul = document.createElement('ul')
document.body.appendChild(divCont)
divCont.appendChild(ul)

fetch('https://swapi.dev/api/films/')
     .then((response) => {
             // console.log(response.json())
             return response.json()
      })
     .then((films) => {
             console.log(films)
             films.results.forEach(film => {
             const filmId = film.episode_id
             console.log(filmId)
             renderFilms(film, filmId)
             getCharacters(film.characters)
                 .then(charactersList => renderCharacters(filmId,charactersList))
             })
             // console.log(films.results)
     })
function renderFilms (film, filmId) {
    console.log(film, filmId)
    const li = document.createElement('li')
    const div = document.createElement('div')
    li.id = filmId
    li.innerHTML = `${film.title}`
    div.innerHTML =`${film.episode_id} <br> ${film.opening_crawl}`
    ul.appendChild(li)
    li.appendChild(div)
}
function getCharacters (links) {
     return new Promise(resolve => {
         const characterList = []
         links.forEach(link => {
             fetch(link)
                 .then(response => response.json())
                 .then(character => {
                     characterList.push(character.name)
                     if(characterList.length === links.length){
                         resolve(characterList)
                     }
                 })
         })
     })
}
function renderCharacters(filmId, charactersList) {
    const episode = document.getElementById(filmId)
    const episodeInfo = episode.querySelector('div')
    const ul = document.createElement('ul')

    episodeInfo.insertAdjacentElement("beforebegin", ul)

    charactersList.forEach(character => {
        const characterItem = document.createElement('li')
        characterItem.innerHTML = character
        ul.appendChild(characterItem)

    })
}