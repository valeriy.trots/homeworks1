import React from 'react';
import {Route, Switch} from "react-router-dom"
import {connect} from 'react-redux';
import {CartList} from './CartList'
import {Modal} from "./Modal";
import {ProductsList} from "./ProductList";
import {
    toggleRemoveModal as toggleRemoveModalAction,
    toggleAddModal as toggleAddModalAction,
} from "../store/ua/actions";
import {
    addProductToCart as addToCart,
    addProductToFavorites as addToFavorites,
    deleteFromCart as removeFromCart,
    deleteFromFavorites as removeFromFavorites
} from "../store/products/action";

const mapStateToProps = (store) => {
    return {
        products: store.products.products,
        productsInCart: store.products.products.filter((product) => product.isInCart),
        productsInFavorites: store.products.products.filter((product) => product.isInFavorites),
        isLoading: store.products.isProductsLoading,
        error: store.products.error,
        isAddModalOpen: store.ui.isAddModalOpen,
        isRemoveModalOpen: store.ui.isRemoveModalOpen,
        currentProductId: store.ui.currentProductId,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProductToCart: (id) => dispatch(addToCart(id)),
        toggleAddModal: (id) => dispatch(toggleAddModalAction(id)),
        toggleRemoveModal: (id) => dispatch(toggleRemoveModalAction(id)),
        addProductToFavorites: (id) => dispatch(addToFavorites(id)),
        deleteFromCart: (id) => dispatch(removeFromCart(id)),
        deleteFromFavorites: (id) => dispatch(removeFromFavorites(id))
    }
}

export const Products = connect(mapStateToProps, mapDispatchToProps)((props) => {
    const {products, isAddModalOpen, toggleAddModal, currentProductId, isRemoveModalOpen} = props;

    return (
        <div>
            <Switch>
                <Route exact path='/'>
                    <Modal isOpen={isAddModalOpen} children={"Добавить товар в корзину?"}
                           onSubmit={() => {
                               props.addProductToCart(currentProductId);
                               toggleAddModal()
                           }}
                           onCancel={() => toggleAddModal()}
                           title='Добавление в корзину'
                    />
                    <ProductsList products={products}
                                  handleAddToFavorites={() => props.addProductToFavorites(currentProductId)}
                                  handleRemoveFromFavorites={() => props.deleteFromFavorites(currentProductId)}
                                  buttonTitle='addToCart'
                    />
                </Route>
                <Route path='/cart'>
                    <Modal
                        isOpen={isRemoveModalOpen}
                        children={"Удалить товар из корзины?"}
                        onSubmit={() => {
                            props.toggleRemoveModal()
                            props.deleteFromCart(currentProductId)
                            console.log(currentProductId)
                        }}
                        onCancel={() => props.toggleRemoveModal()}
                        title='Удалени из корзины'
                    />
                    <CartList
                        products={props.productsInCart}
                        handleAddToFavorites={() => props.addProductToFavorites(currentProductId)}
                        handleRemoveFromFavorites={() => props.deleteFromFavorites(currentProductId)}
                        buttonTitle='addToCart'
                    />
                </Route>
                <Route path='/favorites'>
                    <ProductsList
                        products={products.filter((product) => {
                            if (product.isInFavorites) {
                            }
                            return product.isInFavorites
                        })}
                        handleAddToFavorites={() => props.addProductToFavorites(currentProductId)}
                        handleRemoveFromFavorites={() => props.deleteFromFavorites(currentProductId)}
                        buttonTitle='addToCart'
                    />
                </Route>
            </Switch>

        </div>
    );
})