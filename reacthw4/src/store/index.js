import {createStore,applyMiddleware,compose,combineReducers} from "redux";
import {productsReducer} from './products/reducer.js';
import {uiReducer} from './ua/reducer';
// import {formReducer} from './Form/reducer';
import thunk from "redux-thunk";

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : (f) => f;

const rootReducer = combineReducers({
    products: productsReducer,
    ui: uiReducer,
    // customerInfo: formReducer
});

export const store = createStore(
    rootReducer,
    compose(applyMiddleware(thunk), devTools)
);

export default store;