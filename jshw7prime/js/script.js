function watchArray(arr, parent = document.body) {
    const list = document.createElement('ul');
    const internalList = document.createElement('ul');
    parent.prepend(list);
    arr.map((item) => {
        if (typeof item === 'string') {
            const li = document.createElement('li');
            list.prepend(li);
            li.innerHTML = item;
        } else {
            item.map((interItem) => {
                const interLi = document.createElement('li');
                internalList.prepend(interLi);
                interLi.innerHTML = interItem;
                list.prepend(internalList);
            });
        }
    });
    function removeList() {list.remove()}
    setTimeout(removeList, 3000);
}
watchArray(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);

const div = document.createElement('div');
document.body.prepend(div);
div.style.fontSize = '30px';

function timeCount() { n = 4;
    let timer = setInterval(() =>  { n -= 1;
        div.innerHTML = n;
        if (n === 0) { clearInterval(timer);
            div.innerHTML = '';}
    }, 1000);
}
timeCount();
